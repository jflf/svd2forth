#!/usr/bin/env python3

# ------------------------------------------------------------------------------
#
# svd-forth.py
#
# ------------------------------------------------------------------------------
#
# Extract various peripheral and register definitions from SVD files (part of
# the CMSIS support packages for ARM Cortex microcontrollers) and generate
# Forth code for those definitions.
#
# For more information about CMSIS and SVD:
# https://arm-software.github.io/CMSIS_5/SVD/html/index.html
#
# ------------------------------------------------------------------------------
#
# This file is part of svd-forth <https://gitlab.com/jflf/svd-forth>
# svd-forth Copyright (c) 2020 JFLF
#
# svd-forth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# svd-forth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svd-forth. If not, see <https://www.gnu.org/licenses/>.
#
# ------------------------------------------------------------------------------


import sys
import math
import argparse
import textwrap
import xml.etree.ElementTree as xmlet
from datetime import datetime as dt



# Argument parser

parser = argparse.ArgumentParser( description=textwrap.dedent( '''
Extracts various peripheral and register definitions from SVD files (part of
the CMSIS support packages for ARM Cortex microcontrollers) and generates
Forth code for those definitions.''' ))

#parser.add_argument( '-i', nargs=1, dest='ifile', default=[sys.stdin], help='Read SVD input from IFILE. (default: stdin)' )
parser.add_argument( dest = 'ifile', nargs = 1,  help = 'Read SVD input from IFILE. (use "-" for stdin)' )
parser.add_argument( '-o', nargs = 1, dest = 'ofile', help = 'Write Forth output to OFILE. (default: stdout)' )
#parser.add_argument( '-s', action='store_const', const='|', default='', dest='separator', help='Insert separator "|" for pretty-printing with "column".' )

args = parser.parse_args()

if args.ifile[0] is '-':
    args.ifile[0] = sys.stdin


# Open and load the SVD in one swell swoop

try:
    root = xmlet.parse( args.ifile[0] ).getroot()
except:
    err = sys.exc_info()[0]
    print( 'Error loading the SVD file: {}'.format( err ), file=sys.stderr )
    sys.exit( 1 )


# Open the output file

if args.ofile:
    try:
        sys.stdout = open( args.ofile[0], 'w' )
    except:
        err = sys.exc_info()[0]
        print( 'Error opening the output file: {}'.format( err ), file=sys.stderr )
        sys.exit( 1 )




# ==============================================================================
# Some functions
# ==============================================================================

# ------------------------------------------------------------------------------
# Some functions

# Convert the standard hexadecimal prefix (0x/0X) to Forth ($)

def hex2forth( number ):
    # - we can't do a .lstrip( '0xX' ) as it would also strip non-hex numbers
    # - not worth doing a regexp for that

    num = number.upper()

    if num.startswith( '0X' ):
        return '$' + num[2:].zfill( 4 )
    else:
        return num


# Calculate the display padding size for the first column of a two-element list

def padwidth( list_o_lists ):
    return max( len( i[0] ) for i in list_o_lists ) + 4


# Find if there is a child node with the given tag

def tryfind( node, tag ):
    try:
        return node.find( tag ).text
    except:
        return ''


# Expand arrays of elements
# The rules are cumbersome. Description there:
# https://arm-software.github.io/CMSIS_5/SVD/html/elem_special.html#dimElementGroup_gr
# - name and addressOffset are always defined
# - dim and dimIncrement are always defined for arrays
# - dimIndex is optional
# Note: addressOffset must be in the standard hex format (0x), not Forth

def expandarray( name, addressOffset, dim, dimIncrement, dimIndex ):

    xpanded = []

    # non-array case
    if not ( dim or dimIncrement or dimIndex ):
        return xpanded

    # easier to do the offset computations in decimal
    daddr = int( addressOffset, 0 )
    ddim = int( dim, 0 )
    dinc = int( dimIncrement, 0 )

    pad = int( math.log10( ddim - 1 ) + 1 ) # ddim-1 as we're starting at 0

    if dimIndex:
        dimidx = dimIndex.split( ',' )
        if ddim != len( dimidx ):
            print( 'Invalid array dimension:', name, addressOffset, dim, dimIncrement, dimIndex, file = sys.stderr )
            return []
    else:
        dimidx = None

    for i in range( 0, ddim ):

        # fix the name first
        oname = name

        if oname.endswith( '[%s]' ):
            oname = oname.replace( '[%s]', str( i ).zfill( pad ))

        if dimidx is not None and '%s' in oname:
            oname = oname.replace( '%s', dimidx[ i ].zfill( pad ))

        xpanded.append( [ oname, hex( daddr + i * dinc )])

    return xpanded


# ------------------------------------------------------------------------------
# Extract some general information

header = """
\ Generated by svd-forth on {}
\ svd-forth © 2020 JFLF

""".format( dt.ctime( dt.now() ))


# Stuff that we don't care about at that point
ignored_tags = [ 'licenseText' ]

# Skip ignored items and subnodes
# Subnodes (as in: not leaves, this includes at least "cpu" and "peripherals")
# don't have a text but ElementTree returns a string containing a single
# newline... Strip() will leave that empty, and we'll skip them.
nodes = [ [ i.tag, i.text ] for i in root.getchildren() if i.tag not in ignored_tags and i.text.strip() ]

# dynamically calculate string padding for pretty printing
#padding = max( len( i.tag ) for i in nodes ) + 2
padding = padwidth( nodes )

for i in nodes:
    header += '\n\ {0:{1}}: {2}'.format( i[0], padding, i[1] )




# ==============================================================================
# Parse interesting data and load into dictionaries
# ==============================================================================

# ------------------------------------------------------------------------------
# Dictionary name   # indices

cpus = {}           # [CPU number]
interrupts = {}     # [interrupt number]
peripherals = {}    # [peripheral name]
registers = {}      # [peripheral name][register name]
bitfields = {}      # [peripheral name][register name][bitfield name]



# ------------------------------------------------------------------------------
# CPU(s)

# Currently ignoring the CPUs



# ------------------------------------------------------------------------------
# Interrupts

for i in root.iter( 'interrupt' ):

    # descriptions aren't mandatory
    i_desc = i.find( 'description' )
    i_desc = i_desc.text if i_desc is not None else ''

    interrupts[ int( i.find( 'value' ).text ) ] = [ i.find( 'name' ).text, i_desc ]



# ------------------------------------------------------------------------------
# Peripherals and registers

for p in root.iter( 'peripheral' ):

    # mandatory child elements
    p_name = p.find( 'name' ).text
    p_addr = p.find( 'baseAddress' ).text

    # optional properties and child elements
    p_derived = p.get( 'derivedFrom' )
    p_desc = tryfind( p, 'description' )

    peripherals[ p_name ] = [ hex2forth( p_addr ), p_desc ]

    # If this peripheral is derived from another one, we need to copy the entire
    # register dictionary over, then modify what's redefined.
    # This assumes that the origin peripheral has already been defined!

    if p_derived:
        registers[ p_name ] = registers[ p_derived ].copy()
        bitfields[ p_name ] = bitfields[ p_derived ].copy()
    else:
        registers[ p_name ] = {}
        bitfields[ p_name ] = {}

    # the registers now

    for r in p.findall( './registers/register' ):

        # mandatory child elements
        r_name = r.find( 'name' ).text
        r_offset = r.find( 'addressOffset' ).text

        # optional properties and child elements
        r_desc = tryfind( r, 'description' )
        r_dim = tryfind( r, 'dim' )
        r_inc = tryfind( r, 'dimIncrement' )
        r_idx = tryfind( r, 'dimIndex' )

        registers[ p_name ][ r_name ] = [ hex2forth( r_offset ), r_desc ]

        # let's move on to the bitfields

        # The bitfield dictionary might already exist if that register was
        # defined in the derivedFrom register -- but then it might not! So we
        # always need to check whether it's already been created.
        if r_name not in bitfields[ p_name ].keys():
            bitfields[ p_name ][ r_name ] = {}

        for f in r.findall( './fields/field' ):

            # mandatory child elements
            f_name = f.find( 'name' ).text

            # optional properties and child elements
            f_lsb = tryfind( f, 'lsb' )
            f_msb = tryfind( f, 'msb' )
            f_range = tryfind( f, 'bitRange' )
            f_desc = tryfind( f, 'description' )

            if f_range:
                f_msb, f_lsb = f_range[ 1 : -1 ].split( ':' )

            f_size = int( f_msb ) - int( f_lsb ) + 1
            f_mask = 0

            for i in range( int( f_lsb ), int( f_msb ) + 1 ):
                f_mask = f_mask | ( 1 << i )

            bitfields[ p_name ][ r_name ][ f_name ] = [ f_mask, f_lsb, f_size, f_desc ]

        # If we have a register array we need to duplicate everything.
        # This will only expand to something if we have an array.
        regarray = expandarray( r_name, r_offset, r_dim, r_inc, r_idx )

        if regarray:
            for arr_name, arr_offset in regarray:
                registers[ p_name ][ arr_name ] = [ hex2forth( arr_offset ), r_desc ]
                bitfields[ p_name ][ arr_name ] = bitfields[ p_name ][ r_name ].copy()

            # Once all the copies are made, delete the originals as those have
            # the non-expanded names.
            registers[ p_name ].pop( r_name, None )
            bitfields[ p_name ].pop( r_name, None )




# ==============================================================================
# Print out
# ==============================================================================

# Header

print( header)



print( """

\ INTERRUPTS
\ ----------
""" )

outbuff = []

for i in sorted( interrupts ):
    j = interrupts[i]
    outbuff.append( [ '{} constant INT:{}'.format( i, j[0] ), j[1] ] )

padding = padwidth( outbuff )

for o in outbuff:
    print( '{0:{1}}\ {2}'.format( o[0], padding, o[1] ))



print( """

\ PERIPHERALS
\ -----------
""" )

for p_name in sorted( peripherals, key = lambda x: peripherals[x][0] ):

    q = peripherals[p_name]

    outbuff.clear()
    outbuff.append( [ '{} constant {}'.format( q[0], p_name), q[1] ] )

    for r_name in sorted( registers[ p_name ], key = lambda x: registers[ p_name ][ x ][ 0 ] ):

        s = registers[ p_name ][ r_name ]
        outbuff.append( [ '    {} {} + constant {}:{}'.format( p_name, s[0], p_name, r_name ), s[1] ] )

        for f_name in sorted( bitfields[ p_name ][ r_name ], key = lambda x: int( bitfields[ p_name ][ r_name ][ x ][ 0 ] ) ):

            t = bitfields[ p_name ][ r_name ][ f_name ]
            outbuff.append( [ '        %{0:0>32b} {1}:{2} bitfield {1}:{2}:{3}'.format( t[0], p_name, r_name, f_name ), t[3] ] )

    padding = padwidth( outbuff )

    for o in outbuff:
        print( '{0:{1}}\ {2}'.format( o[0], padding, o[1] ))

    print()

