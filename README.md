# svd-forth

## Descriptions

`svd-forth` is a Python script that converts ARM SVD peripheral and register information to Forth definitions.

It works reasonably well on some of the SVD files that I have tested, but there's so much variation from vendor to vendor that the output needs to be double-checked every time. And let's not get started on the errors and incorrect declarations in those files.

## WARNING

I haven't nailed down yet how to manage bitfields effectively. Right now there's a defining word called `bitfield` that I use as a placeholder, but I don't have a definition for it. The bitfield definitions are very likely to change at some point.


## User guide

Yup that's all of it:

```
$ ./svd-forth.py -h
usage: svd-forth.py [-h] [-o OFILE] ifile

Extracts various peripheral and register definitions from SVD files (part of
the CMSIS support packages for ARM Cortex microcontrollers) and generates
Forth code for those definitions.

positional arguments:
  ifile       Read SVD input from IFILE. (use "-" for stdin)

optional arguments:
  -h, --help  show this help message and exit
  -o OFILE    Write Forth output to OFILE. (default: stdout)
```

## Example outputs

The output from the [example ARM SVD file](https://arm-software.github.io/CMSIS_5/SVD/html/svd_Example_pg.html) gives this:

```
\ Generated by svd-forth on Tue Apr 21 22:47:24 2020
\ svd-forth © 2020 JFLF


\ vendor             : ARM Ltd.
\ vendorID           : ARM
\ name               : ARM_Example
\ series             : ARMCM3
\ version            : 1.2
\ description        : ARM 32-bit Cortex-M3 Microcontroller based device, CPU clock up to 80MHz, etc. 
\ addressUnitBits    : 8
\ width              : 32
\ size               : 32
\ access             : read-write
\ resetValue         : 0x00000000
\ resetMask          : 0xFFFFFFFF


\ INTERRUPTS
\ ----------

0 constant INT:TIMER0    \ Timer 0 interrupt
4 constant INT:TIMER1    \ Timer 2 interrupt
6 constant INT:TIMER2    \ Timer 2 interrupt


\ PERIPHERALS
\ -----------

$40010000 constant TIMER0                                                         \ 32 Timer / Counter, counting up or down from different sources
    TIMER0 $0000 + constant TIMER0:CR                                             \ Control Register
        %00000000000000000000000000000001 TIMER0:CR bitfield TIMER0:CR:EN         \ Enable
        %00000000000000000000000000000010 TIMER0:CR bitfield TIMER0:CR:RST        \ Reset Timer
        %00000000000000000000000000001100 TIMER0:CR bitfield TIMER0:CR:CNT        \ Counting direction
        %00000000000000000000000001110000 TIMER0:CR bitfield TIMER0:CR:MODE       \ Operation Mode
        %00000000000000000000000010000000 TIMER0:CR bitfield TIMER0:CR:PSC        \ Use Prescaler
        %00000000000000000000111100000000 TIMER0:CR bitfield TIMER0:CR:CNTSRC     \ Timer / Counter Source Divider
        %00000000000000001111000000000000 TIMER0:CR bitfield TIMER0:CR:CAPSRC     \ Timer / Counter Capture Source
        %00000000000000110000000000000000 TIMER0:CR bitfield TIMER0:CR:CAPEDGE    \ Capture Edge, select which Edge should result in a counter increment or decrement
        %00000000001100000000000000000000 TIMER0:CR bitfield TIMER0:CR:TRGEXT     \ Triggers an other Peripheral
        %00000011000000000000000000000000 TIMER0:CR bitfield TIMER0:CR:RELOAD     \ Select RELOAD Register n to reload Timer on condition
        %00001100000000000000000000000000 TIMER0:CR bitfield TIMER0:CR:IDR        \ Selects, if Reload Register number is incremented, decremented or not modified
        %10000000000000000000000000000000 TIMER0:CR bitfield TIMER0:CR:S          \ Starts and Stops the Timer / Counter
    TIMER0 $0004 + constant TIMER0:SR                                             \ Status Register
        %00000000000000000000000000000001 TIMER0:SR bitfield TIMER0:SR:RUN        \ Shows if Timer is running or not
        %00000000000000000000000100000000 TIMER0:SR bitfield TIMER0:SR:MATCH      \ Shows if the MATCH was hit
        %00000000000000000000001000000000 TIMER0:SR bitfield TIMER0:SR:UN         \ Shows if an underflow occured. This flag is sticky
        %00000000000000000000010000000000 TIMER0:SR bitfield TIMER0:SR:OV         \ Shows if an overflow occured. This flag is sticky
        %00000000000000000001000000000000 TIMER0:SR bitfield TIMER0:SR:RST        \ Shows if Timer is in RESET state
        %00000000000000001100000000000000 TIMER0:SR bitfield TIMER0:SR:RELOAD     \ Shows the currently active RELOAD Register
    TIMER0 $0010 + constant TIMER0:INT                                            \ Interrupt Register
        %00000000000000000000000000000001 TIMER0:INT bitfield TIMER0:INT:EN       \ Interrupt Enable
        %00000000000000000000000001110000 TIMER0:INT bitfield TIMER0:INT:MODE     \ Interrupt Mode, selects on which condition the Timer should generate an Interrupt
    TIMER0 $0020 + constant TIMER0:COUNT                                          \ The Counter Register reflects the actual Value of the Timer/Counter
    TIMER0 $0024 + constant TIMER0:MATCH                                          \ The Match Register stores the compare Value for the MATCH condition
    TIMER0 $0028 + constant TIMER0:PRESCALE_RD                                    \ The Prescale Register stores the Value for the prescaler. The cont event gets divided by this value
    TIMER0 $0028 + constant TIMER0:PRESCALE_WR                                    \ The Prescale Register stores the Value for the prescaler. The cont event gets divided by this value
    TIMER0 $0050 + constant TIMER0:RELOAD0                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER0 $0054 + constant TIMER0:RELOAD1                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER0 $0058 + constant TIMER0:RELOAD2                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER0 $005C + constant TIMER0:RELOAD3                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.

$40010100 constant TIMER1                                                         \ 
    TIMER1 $0000 + constant TIMER1:CR                                             \ Control Register
        %00000000000000000000000000000001 TIMER1:CR bitfield TIMER1:CR:EN         \ Enable
        %00000000000000000000000000000010 TIMER1:CR bitfield TIMER1:CR:RST        \ Reset Timer
        %00000000000000000000000000001100 TIMER1:CR bitfield TIMER1:CR:CNT        \ Counting direction
        %00000000000000000000000001110000 TIMER1:CR bitfield TIMER1:CR:MODE       \ Operation Mode
        %00000000000000000000000010000000 TIMER1:CR bitfield TIMER1:CR:PSC        \ Use Prescaler
        %00000000000000000000111100000000 TIMER1:CR bitfield TIMER1:CR:CNTSRC     \ Timer / Counter Source Divider
        %00000000000000001111000000000000 TIMER1:CR bitfield TIMER1:CR:CAPSRC     \ Timer / Counter Capture Source
        %00000000000000110000000000000000 TIMER1:CR bitfield TIMER1:CR:CAPEDGE    \ Capture Edge, select which Edge should result in a counter increment or decrement
        %00000000001100000000000000000000 TIMER1:CR bitfield TIMER1:CR:TRGEXT     \ Triggers an other Peripheral
        %00000011000000000000000000000000 TIMER1:CR bitfield TIMER1:CR:RELOAD     \ Select RELOAD Register n to reload Timer on condition
        %00001100000000000000000000000000 TIMER1:CR bitfield TIMER1:CR:IDR        \ Selects, if Reload Register number is incremented, decremented or not modified
        %10000000000000000000000000000000 TIMER1:CR bitfield TIMER1:CR:S          \ Starts and Stops the Timer / Counter
    TIMER1 $0004 + constant TIMER1:SR                                             \ Status Register
        %00000000000000000000000000000001 TIMER1:SR bitfield TIMER1:SR:RUN        \ Shows if Timer is running or not
        %00000000000000000000000100000000 TIMER1:SR bitfield TIMER1:SR:MATCH      \ Shows if the MATCH was hit
        %00000000000000000000001000000000 TIMER1:SR bitfield TIMER1:SR:UN         \ Shows if an underflow occured. This flag is sticky
        %00000000000000000000010000000000 TIMER1:SR bitfield TIMER1:SR:OV         \ Shows if an overflow occured. This flag is sticky
        %00000000000000000001000000000000 TIMER1:SR bitfield TIMER1:SR:RST        \ Shows if Timer is in RESET state
        %00000000000000001100000000000000 TIMER1:SR bitfield TIMER1:SR:RELOAD     \ Shows the currently active RELOAD Register
    TIMER1 $0010 + constant TIMER1:INT                                            \ Interrupt Register
        %00000000000000000000000000000001 TIMER1:INT bitfield TIMER1:INT:EN       \ Interrupt Enable
        %00000000000000000000000001110000 TIMER1:INT bitfield TIMER1:INT:MODE     \ Interrupt Mode, selects on which condition the Timer should generate an Interrupt
    TIMER1 $0020 + constant TIMER1:COUNT                                          \ The Counter Register reflects the actual Value of the Timer/Counter
    TIMER1 $0024 + constant TIMER1:MATCH                                          \ The Match Register stores the compare Value for the MATCH condition
    TIMER1 $0028 + constant TIMER1:PRESCALE_RD                                    \ The Prescale Register stores the Value for the prescaler. The cont event gets divided by this value
    TIMER1 $0028 + constant TIMER1:PRESCALE_WR                                    \ The Prescale Register stores the Value for the prescaler. The cont event gets divided by this value
    TIMER1 $0050 + constant TIMER1:RELOAD0                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER1 $0054 + constant TIMER1:RELOAD1                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER1 $0058 + constant TIMER1:RELOAD2                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER1 $005C + constant TIMER1:RELOAD3                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.

$40010200 constant TIMER2                                                         \ 
    TIMER2 $0000 + constant TIMER2:CR                                             \ Control Register
        %00000000000000000000000000000001 TIMER2:CR bitfield TIMER2:CR:EN         \ Enable
        %00000000000000000000000000000010 TIMER2:CR bitfield TIMER2:CR:RST        \ Reset Timer
        %00000000000000000000000000001100 TIMER2:CR bitfield TIMER2:CR:CNT        \ Counting direction
        %00000000000000000000000001110000 TIMER2:CR bitfield TIMER2:CR:MODE       \ Operation Mode
        %00000000000000000000000010000000 TIMER2:CR bitfield TIMER2:CR:PSC        \ Use Prescaler
        %00000000000000000000111100000000 TIMER2:CR bitfield TIMER2:CR:CNTSRC     \ Timer / Counter Source Divider
        %00000000000000001111000000000000 TIMER2:CR bitfield TIMER2:CR:CAPSRC     \ Timer / Counter Capture Source
        %00000000000000110000000000000000 TIMER2:CR bitfield TIMER2:CR:CAPEDGE    \ Capture Edge, select which Edge should result in a counter increment or decrement
        %00000000001100000000000000000000 TIMER2:CR bitfield TIMER2:CR:TRGEXT     \ Triggers an other Peripheral
        %00000011000000000000000000000000 TIMER2:CR bitfield TIMER2:CR:RELOAD     \ Select RELOAD Register n to reload Timer on condition
        %00001100000000000000000000000000 TIMER2:CR bitfield TIMER2:CR:IDR        \ Selects, if Reload Register number is incremented, decremented or not modified
        %10000000000000000000000000000000 TIMER2:CR bitfield TIMER2:CR:S          \ Starts and Stops the Timer / Counter
    TIMER2 $0004 + constant TIMER2:SR                                             \ Status Register
        %00000000000000000000000000000001 TIMER2:SR bitfield TIMER2:SR:RUN        \ Shows if Timer is running or not
        %00000000000000000000000100000000 TIMER2:SR bitfield TIMER2:SR:MATCH      \ Shows if the MATCH was hit
        %00000000000000000000001000000000 TIMER2:SR bitfield TIMER2:SR:UN         \ Shows if an underflow occured. This flag is sticky
        %00000000000000000000010000000000 TIMER2:SR bitfield TIMER2:SR:OV         \ Shows if an overflow occured. This flag is sticky
        %00000000000000000001000000000000 TIMER2:SR bitfield TIMER2:SR:RST        \ Shows if Timer is in RESET state
        %00000000000000001100000000000000 TIMER2:SR bitfield TIMER2:SR:RELOAD     \ Shows the currently active RELOAD Register
    TIMER2 $0010 + constant TIMER2:INT                                            \ Interrupt Register
        %00000000000000000000000000000001 TIMER2:INT bitfield TIMER2:INT:EN       \ Interrupt Enable
        %00000000000000000000000001110000 TIMER2:INT bitfield TIMER2:INT:MODE     \ Interrupt Mode, selects on which condition the Timer should generate an Interrupt
    TIMER2 $0020 + constant TIMER2:COUNT                                          \ The Counter Register reflects the actual Value of the Timer/Counter
    TIMER2 $0024 + constant TIMER2:MATCH                                          \ The Match Register stores the compare Value for the MATCH condition
    TIMER2 $0028 + constant TIMER2:PRESCALE_RD                                    \ The Prescale Register stores the Value for the prescaler. The cont event gets divided by this value
    TIMER2 $0028 + constant TIMER2:PRESCALE_WR                                    \ The Prescale Register stores the Value for the prescaler. The cont event gets divided by this value
    TIMER2 $0050 + constant TIMER2:RELOAD0                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER2 $0054 + constant TIMER2:RELOAD1                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER2 $0058 + constant TIMER2:RELOAD2                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
    TIMER2 $005C + constant TIMER2:RELOAD3                                        \ The Reload Register stores the Value the COUNT Register gets reloaded on a when a condition was met.
```

